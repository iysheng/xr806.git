EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic_MountingPin:Conn_01x24_MountingPin J2
U 1 1 61CD760F
P 7800 2800
F 0 "J2" H 7888 2714 50  0000 L CNN
F 1 "Conn_01x24_MountingPin" H 7888 2623 50  0000 L CNN
F 2 "Connector_FFC-FPC:Hirose_FH12-24S-0.5SH_1x24-1MP_P0.50mm_Horizontal" H 7800 2800 50  0001 C CNN
F 3 "~" H 7800 2800 50  0001 C CNN
	1    7800 2800
	1    0    0    -1  
$EndComp
NoConn ~ 7600 1700
NoConn ~ 7600 2000
NoConn ~ 7600 2200
NoConn ~ 7600 2300
NoConn ~ 7600 3500
$Comp
L power:GND #PWR0101
U 1 1 61CDA320
P 7800 4250
F 0 "#PWR0101" H 7800 4000 50  0001 C CNN
F 1 "GND" H 7805 4077 50  0000 C CNN
F 2 "" H 7800 4250 50  0001 C CNN
F 3 "" H 7800 4250 50  0001 C CNN
	1    7800 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4200 7800 4250
Wire Wire Line
	7600 1800 7250 1800
Wire Wire Line
	7600 1900 7250 1900
Text Label 7300 1800 0    50   ~ 0
GDR
Text Label 7300 1900 0    50   ~ 0
RESE
Wire Wire Line
	7600 2100 7250 2100
$Comp
L Device:C C11
U 1 1 61CDD6D8
P 7100 2100
F 0 "C11" V 7050 2000 50  0000 C CNN
F 1 "C" V 7150 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7138 1950 50  0001 C CNN
F 3 "~" H 7100 2100 50  0001 C CNN
	1    7100 2100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 61CDF6F7
P 6850 2100
F 0 "#PWR0102" H 6850 1850 50  0001 C CNN
F 1 "GND" V 6855 1972 50  0000 R CNN
F 2 "" H 6850 2100 50  0001 C CNN
F 3 "" H 6850 2100 50  0001 C CNN
	1    6850 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 2100 6900 2100
Wire Wire Line
	6900 2400 6900 2100
Wire Wire Line
	6900 2400 7600 2400
Connection ~ 6900 2100
Wire Wire Line
	6900 2100 6850 2100
Wire Wire Line
	7600 2500 7250 2500
Wire Wire Line
	7600 2600 7250 2600
Wire Wire Line
	7600 2700 7250 2700
Wire Wire Line
	7600 2800 7250 2800
Text Label 7300 2500 0    50   ~ 0
BUSY
Text Label 7300 2600 0    50   ~ 0
RES
Text Label 7300 2700 0    50   ~ 0
D_C
Text Label 7300 2800 0    50   ~ 0
CS
Wire Wire Line
	7600 2900 7250 2900
Wire Wire Line
	7600 3000 7250 3000
Text Label 7300 2900 0    50   ~ 0
SCLK
Text Label 7300 3000 0    50   ~ 0
SDI
$Comp
L Device:C C6
U 1 1 61CE5E17
P 7050 3200
F 0 "C6" V 7000 3100 50  0000 C CNN
F 1 "1uf" V 7100 3100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7088 3050 50  0001 C CNN
F 3 "~" H 7050 3200 50  0001 C CNN
	1    7050 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	7600 3200 7350 3200
Wire Wire Line
	7600 3100 7350 3100
Wire Wire Line
	7350 3100 7350 3200
Connection ~ 7350 3200
Wire Wire Line
	7350 3200 7200 3200
$Comp
L power:+3V3 #PWR0103
U 1 1 61CE69DD
P 7350 3100
F 0 "#PWR0103" H 7350 2950 50  0001 C CNN
F 1 "+3V3" V 7365 3228 50  0000 L CNN
F 2 "" H 7350 3100 50  0001 C CNN
F 3 "" H 7350 3100 50  0001 C CNN
	1    7350 3100
	0    -1   -1   0   
$EndComp
Connection ~ 7350 3100
$Comp
L power:GND #PWR0104
U 1 1 61CE71D3
P 6800 3200
F 0 "#PWR0104" H 6800 2950 50  0001 C CNN
F 1 "GND" V 6805 3072 50  0000 R CNN
F 2 "" H 6800 3200 50  0001 C CNN
F 3 "" H 6800 3200 50  0001 C CNN
	1    6800 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 3200 6850 3200
Wire Wire Line
	7600 3300 6850 3300
Wire Wire Line
	6850 3300 6850 3200
Connection ~ 6850 3200
Wire Wire Line
	6850 3200 6800 3200
$Comp
L Device:C C7
U 1 1 61CE81F8
P 7050 3400
F 0 "C7" V 7000 3300 50  0000 C CNN
F 1 "1uf" V 7100 3300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7088 3250 50  0001 C CNN
F 3 "~" H 7050 3400 50  0001 C CNN
	1    7050 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	7600 3400 7200 3400
Wire Wire Line
	6900 3400 6850 3400
Wire Wire Line
	6850 3400 6850 3300
Connection ~ 6850 3300
$Comp
L Device:C C8
U 1 1 61CE901A
P 7050 3600
F 0 "C8" V 7000 3500 50  0000 C CNN
F 1 "1uf" V 7100 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7088 3450 50  0001 C CNN
F 3 "~" H 7050 3600 50  0001 C CNN
	1    7050 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	7600 3600 7200 3600
Wire Wire Line
	6900 3600 6850 3600
Wire Wire Line
	6850 3600 6850 3400
Connection ~ 6850 3400
Wire Wire Line
	7600 3700 7200 3700
Text Label 7250 3700 0    50   ~ 0
PREVGH
$Comp
L Device:C C9
U 1 1 61CEB3DF
P 7050 3800
F 0 "C9" V 7000 3700 50  0000 C CNN
F 1 "1uf" V 7100 3700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7088 3650 50  0001 C CNN
F 3 "~" H 7050 3800 50  0001 C CNN
	1    7050 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	7600 3800 7200 3800
Text Label 7250 3900 0    50   ~ 0
PREVGL
$Comp
L Device:C C5
U 1 1 61CEC9CE
P 6850 3900
F 0 "C5" V 6800 3800 50  0000 C CNN
F 1 "1uf" V 6900 3800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 6888 3750 50  0001 C CNN
F 3 "~" H 6850 3900 50  0001 C CNN
	1    6850 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 3900 7600 3900
$Comp
L Device:C C10
U 1 1 61CEDE04
P 7050 4000
F 0 "C10" V 7000 3900 50  0000 C CNN
F 1 "1uf" V 7100 3900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7088 3850 50  0001 C CNN
F 3 "~" H 7050 4000 50  0001 C CNN
	1    7050 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 4000 7600 4000
Wire Wire Line
	6900 4000 6650 4000
Wire Wire Line
	6650 4000 6650 3900
Wire Wire Line
	6650 3900 6700 3900
Wire Wire Line
	6900 3800 6650 3800
Wire Wire Line
	6650 3800 6650 3900
Connection ~ 6650 3900
Wire Wire Line
	6850 3600 6650 3600
Wire Wire Line
	6650 3600 6650 3800
Connection ~ 6850 3600
Connection ~ 6650 3800
Text Notes 7900 3100 0    50   Italic 0
FPC24 0.5mm
$Comp
L Diode:MBR0530 D1
U 1 1 61CF548D
P 5700 2300
F 0 "D1" H 5700 2517 50  0000 C CNN
F 1 "MBR0530" H 5700 2426 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 5700 2125 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 5700 2300 50  0001 C CNN
	1    5700 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 2300 6200 2300
Text Label 5900 2300 0    50   ~ 0
PREVGL
$Comp
L Diode:MBR0530 D2
U 1 1 61CF7A15
P 5700 2600
F 0 "D2" H 5700 2383 50  0000 C CNN
F 1 "MBR0530" H 5700 2474 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 5700 2425 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 5700 2600 50  0001 C CNN
	1    5700 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 2600 6200 2600
$Comp
L power:GND #PWR0105
U 1 1 61CF8F89
P 6200 2600
F 0 "#PWR0105" H 6200 2350 50  0001 C CNN
F 1 "GND" V 6205 2472 50  0000 R CNN
F 2 "" H 6200 2600 50  0001 C CNN
F 3 "" H 6200 2600 50  0001 C CNN
	1    6200 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 2300 5500 2300
Wire Wire Line
	5500 2300 5500 2600
Wire Wire Line
	5500 2600 5550 2600
$Comp
L Device:C C3
U 1 1 61CFB1F3
P 5300 2600
F 0 "C3" V 5250 2500 50  0000 C CNN
F 1 "1uf" V 5350 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5338 2450 50  0001 C CNN
F 3 "~" H 5300 2600 50  0001 C CNN
	1    5300 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5300 2450 5300 2300
Wire Wire Line
	5300 2300 5500 2300
Connection ~ 5500 2300
$Comp
L Diode:MBR0530 D3
U 1 1 61CFC91E
P 5700 2950
F 0 "D3" H 5700 2733 50  0000 C CNN
F 1 "MBR0530" H 5700 2824 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 5700 2775 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 5700 2950 50  0001 C CNN
	1    5700 2950
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 2950 6000 2950
Text Label 5900 2950 0    50   ~ 0
PREVGH
$Comp
L Device:C C4
U 1 1 61CFE650
P 6000 3150
F 0 "C4" V 5950 3050 50  0000 C CNN
F 1 "1uf" V 6050 3050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 6038 3000 50  0001 C CNN
F 3 "~" H 6000 3150 50  0001 C CNN
	1    6000 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 3000 6000 2950
Connection ~ 6000 2950
Wire Wire Line
	6000 2950 6200 2950
$Comp
L power:GND #PWR0106
U 1 1 61D00507
P 6000 3450
F 0 "#PWR0106" H 6000 3200 50  0001 C CNN
F 1 "GND" H 6005 3277 50  0000 C CNN
F 2 "" H 6000 3450 50  0001 C CNN
F 3 "" H 6000 3450 50  0001 C CNN
	1    6000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3450 6000 3400
Wire Wire Line
	5550 2950 5300 2950
Wire Wire Line
	5300 2950 5300 2750
$Comp
L Device:L L2
U 1 1 61D074CD
P 4700 2950
F 0 "L2" V 4890 2950 50  0000 C CNN
F 1 "10uh" V 4799 2950 50  0000 C CNN
F 2 "Inductor_SMD:L_Coilcraft_XAL60xx_6.36x6.56mm" H 4700 2950 50  0001 C CNN
F 3 "~" H 4700 2950 50  0001 C CNN
	1    4700 2950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4850 2950 5300 2950
Connection ~ 5300 2950
$Comp
L Device:C C2
U 1 1 61D09A80
P 4350 3200
F 0 "C2" V 4300 3100 50  0000 C CNN
F 1 "4.7uf" V 4400 3050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4388 3050 50  0001 C CNN
F 3 "~" H 4350 3200 50  0001 C CNN
	1    4350 3200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 2950 4350 2950
Wire Wire Line
	4350 2950 4350 3050
$Comp
L power:GND #PWR0107
U 1 1 61D0CDF9
P 4350 3450
F 0 "#PWR0107" H 4350 3200 50  0001 C CNN
F 1 "GND" H 4355 3277 50  0000 C CNN
F 2 "" H 4350 3450 50  0001 C CNN
F 3 "" H 4350 3450 50  0001 C CNN
	1    4350 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3450 4350 3350
$Comp
L power:+3V3 #PWR0108
U 1 1 61D0EBBF
P 4350 2850
F 0 "#PWR0108" H 4350 2700 50  0001 C CNN
F 1 "+3V3" H 4365 3023 50  0000 C CNN
F 2 "" H 4350 2850 50  0001 C CNN
F 3 "" H 4350 2850 50  0001 C CNN
	1    4350 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2850 4350 2950
Connection ~ 4350 2950
$Comp
L RedLib:Si1308EDL-T1-GE3 Q1
U 1 1 61D13978
P 5000 4050
F 0 "Q1" H 5430 4196 50  0000 L CNN
F 1 "Si1308EDL-T1-GE3" H 5430 4105 50  0000 L CNN
F 2 "RedLib:SOT65P210X110-3N_Si1308" H 5450 4000 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/SI1308EDL-T1-GE3.pdf" H 5450 3900 50  0001 L CNN
F 4 "MOSFET N-Ch 30V 1.5A TrenchFET SC70 Vishay Si1308EDL-T1-GE3 N-channel MOSFET Transistor, 1.5 A, 30 V, 3-Pin SC-70" H 5450 3800 50  0001 L CNN "Description"
F 5 "1.1" H 5450 3700 50  0001 L CNN "Height"
F 6 "Vishay" H 5450 3600 50  0001 L CNN "Manufacturer_Name"
F 7 "Si1308EDL-T1-GE3" H 5450 3500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "78-SI1308EDL-T1-GE3" H 5450 3400 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Vishay-Semiconductors/SI1308EDL-T1-GE3?qs=bX1%252BNvsK%2FBramh9tgpOaEw%3D%3D" H 5450 3300 50  0001 L CNN "Mouser Price/Stock"
F 10 "SI1308EDL-T1-GE3" H 5450 3200 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/si1308edl-t1-ge3/vishay?region=nac" H 5450 3100 50  0001 L CNN "Arrow Price/Stock"
	1    5000 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3650 5300 2950
$Comp
L Device:R R3
U 1 1 61D1B2E0
P 5300 4650
F 0 "R3" H 5370 4696 50  0000 L CNN
F 1 "3" H 5370 4605 50  0000 L CNN
F 2 "RedLib:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5230 4650 50  0001 C CNN
F 3 "~" H 5300 4650 50  0001 C CNN
	1    5300 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4500 5300 4250
$Comp
L Device:R R2
U 1 1 61D1E450
P 4900 4650
F 0 "R2" H 4970 4696 50  0000 L CNN
F 1 "10k" H 4970 4605 50  0000 L CNN
F 2 "RedLib:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4830 4650 50  0001 C CNN
F 3 "~" H 4900 4650 50  0001 C CNN
	1    4900 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4050 4900 4050
Wire Wire Line
	4900 4050 4900 4500
$Comp
L power:GND #PWR0109
U 1 1 61D204A2
P 5100 4950
F 0 "#PWR0109" H 5100 4700 50  0001 C CNN
F 1 "GND" H 5105 4777 50  0000 C CNN
F 2 "" H 5100 4950 50  0001 C CNN
F 3 "" H 5100 4950 50  0001 C CNN
	1    5100 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4800 4900 4900
Wire Wire Line
	4900 4900 5100 4900
Wire Wire Line
	5300 4900 5300 4800
Wire Wire Line
	5100 4950 5100 4900
Connection ~ 5100 4900
Wire Wire Line
	5100 4900 5300 4900
Text Label 4950 4050 0    50   ~ 0
GDR
Text Label 5300 4350 0    50   ~ 0
RESE
Wire Wire Line
	2700 3750 3050 3750
Wire Wire Line
	2700 3950 3050 3950
Wire Wire Line
	2700 4050 3050 4050
Text Label 2800 3750 0    50   ~ 0
SCLK
Text Label 2800 3950 0    50   ~ 0
CS
Text Label 2800 4050 0    50   ~ 0
SDI
Wire Wire Line
	2700 3650 3050 3650
Text Label 2800 3650 0    50   ~ 0
BUSY
Wire Wire Line
	1850 3650 1550 3650
Wire Wire Line
	1850 3550 1550 3550
Text Label 1600 3550 0    50   ~ 0
D_C
Text Label 1600 3650 0    50   ~ 0
RES
$Comp
L power:GND #PWR0110
U 1 1 61D3F3D9
P 3000 4150
F 0 "#PWR0110" H 3000 3900 50  0001 C CNN
F 1 "GND" V 3005 4022 50  0000 R CNN
F 2 "" H 3000 4150 50  0001 C CNN
F 3 "" H 3000 4150 50  0001 C CNN
	1    3000 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 4150 2700 4150
Wire Wire Line
	2700 4250 2850 4250
$Comp
L power:+3V3 #PWR0111
U 1 1 61D44469
P 3000 4250
F 0 "#PWR0111" H 3000 4100 50  0001 C CNN
F 1 "+3V3" V 3015 4378 50  0000 L CNN
F 2 "" H 3000 4250 50  0001 C CNN
F 3 "" H 3000 4250 50  0001 C CNN
	1    3000 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 3550 3050 3550
Text Label 2800 3550 0    50   ~ 0
RX1
Text Label 2800 3450 0    50   ~ 0
TX1
Text Notes 1050 4050 1    50   Italic 0
SPI PORT
Wire Wire Line
	1850 3750 1350 3750
Wire Wire Line
	1850 3850 1350 3850
Wire Wire Line
	1350 3950 1850 3950
Wire Wire Line
	1850 4050 1350 4050
Wire Wire Line
	1850 4150 1550 4150
Text Label 1600 4150 0    50   ~ 0
IR_RX
Text Label 1400 3750 0    50   ~ 0
SPI0_MOSI
Text Label 1400 3850 0    50   ~ 0
SPI0_MISO
Text Label 1400 3950 0    50   ~ 0
SPI0_CS0
Text Label 1400 4050 0    50   ~ 0
SPI0_CLK
Wire Notes Line
	1375 3675 1825 3675
Wire Notes Line
	1825 3675 1825 4075
Wire Notes Line
	1825 4075 1375 4075
Wire Notes Line
	1375 4075 1375 3675
NoConn ~ 2700 3850
NoConn ~ 1850 3450
$Comp
L RedLib:IRM-H638T L1
U 1 1 61D91D81
P 1150 4800
F 0 "L1" H 1375 5075 50  0000 C CNN
F 1 "IRM-H638T" H 1375 4984 50  0000 C CNN
F 2 "RedLib:SOIC-4-INFRA" H 1600 5000 50  0001 C CNN
F 3 "https://item.taobao.com/item.htm?spm=a230r.1.14.18.4d371e744mxNCA&id=568118730734&ns=1&abbucket=16#detail" H 1600 5000 50  0001 C CNN
	1    1150 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 61D928FB
P 950 5400
F 0 "#PWR0112" H 950 5150 50  0001 C CNN
F 1 "GND" H 955 5227 50  0000 C CNN
F 2 "" H 950 5400 50  0001 C CNN
F 3 "" H 950 5400 50  0001 C CNN
	1    950  5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 4850 950  4850
Wire Wire Line
	950  4850 950  5400
Wire Wire Line
	1050 4750 950  4750
Wire Wire Line
	950  4750 950  4850
Connection ~ 950  4850
$Comp
L power:+3V3 #PWR0113
U 1 1 61D99387
P 2300 4700
F 0 "#PWR0113" H 2300 4550 50  0001 C CNN
F 1 "+3V3" H 2315 4873 50  0000 C CNN
F 2 "" H 2300 4700 50  0001 C CNN
F 3 "" H 2300 4700 50  0001 C CNN
	1    2300 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 61DA0E03
P 2000 4750
F 0 "R1" V 1793 4750 50  0000 C CNN
F 1 "100" V 1884 4750 50  0000 C CNN
F 2 "RedLib:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 1930 4750 50  0001 C CNN
F 3 "~" H 2000 4750 50  0001 C CNN
	1    2000 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 4750 1800 4750
Wire Wire Line
	2300 4700 2300 4750
Wire Wire Line
	2300 4750 2150 4750
$Comp
L Device:C C1
U 1 1 61DA884E
P 1800 5100
F 0 "C1" V 1750 5000 50  0000 C CNN
F 1 "4.7uf" V 1850 4950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1838 4950 50  0001 C CNN
F 3 "~" H 1800 5100 50  0001 C CNN
	1    1800 5100
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 4950 1800 4750
Connection ~ 1800 4750
Wire Wire Line
	1800 4750 1850 4750
$Comp
L power:GND #PWR0114
U 1 1 61DB01D0
P 1800 5400
F 0 "#PWR0114" H 1800 5150 50  0001 C CNN
F 1 "GND" H 1805 5227 50  0000 C CNN
F 2 "" H 1800 5400 50  0001 C CNN
F 3 "" H 1800 5400 50  0001 C CNN
	1    1800 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 5400 1800 5250
Text Label 1850 4850 0    50   ~ 0
IR_RX
$Comp
L Connector_Generic:Conn_01x06 J1
U 1 1 61DB9FCC
P 1150 3850
F 0 "J1" H 1068 4267 50  0000 C CNN
F 1 "Conn_01x06" H 1068 4176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 1150 3850 50  0001 C CNN
F 3 "~" H 1150 3850 50  0001 C CNN
	1    1150 3850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1350 3650 1400 3650
Wire Wire Line
	1400 3650 1400 3300
$Comp
L power:GND #PWR0115
U 1 1 61DCB345
P 1400 3300
F 0 "#PWR0115" H 1400 3050 50  0001 C CNN
F 1 "GND" H 1405 3127 50  0000 C CNN
F 2 "" H 1400 3300 50  0001 C CNN
F 3 "" H 1400 3300 50  0001 C CNN
	1    1400 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 4850 2000 4850
$Comp
L RedLib:XR806 U1
U 1 1 61CD585F
P 1950 4300
F 0 "U1" H 2275 5375 50  0000 C CNN
F 1 "XR806" H 2275 5284 50  0000 C CNN
F 2 "RedLib:DIP-18_913_XR806" H 1850 5050 50  0001 C CNN
F 3 "" H 1850 5050 50  0001 C CNN
	1    1950 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 4250 1600 4250
Wire Wire Line
	1450 4250 1450 4150
Wire Wire Line
	1450 4150 1350 4150
$Comp
L power:+5V #PWR0116
U 1 1 61DD7D8F
P 1450 4250
F 0 "#PWR0116" H 1450 4100 50  0001 C CNN
F 1 "+5V" V 1465 4378 50  0000 L CNN
F 2 "" H 1450 4250 50  0001 C CNN
F 3 "" H 1450 4250 50  0001 C CNN
	1    1450 4250
	0    -1   -1   0   
$EndComp
Connection ~ 1450 4250
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 61DDA3B7
P 2850 4250
F 0 "#FLG0101" H 2850 4325 50  0001 C CNN
F 1 "PWR_FLAG" H 2850 4423 50  0000 C CNN
F 2 "" H 2850 4250 50  0001 C CNN
F 3 "~" H 2850 4250 50  0001 C CNN
	1    2850 4250
	-1   0    0    1   
$EndComp
Connection ~ 2850 4250
Wire Wire Line
	2850 4250 3000 4250
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 61DDA8FA
P 1600 4250
F 0 "#FLG0102" H 1600 4325 50  0001 C CNN
F 1 "PWR_FLAG" H 1600 4423 50  0000 C CNN
F 2 "" H 1600 4250 50  0001 C CNN
F 3 "~" H 1600 4250 50  0001 C CNN
	1    1600 4250
	-1   0    0    1   
$EndComp
Connection ~ 1600 4250
Wire Wire Line
	1600 4250 1450 4250
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 61DDBC0C
P 6000 3400
F 0 "#FLG0103" H 6000 3475 50  0001 C CNN
F 1 "PWR_FLAG" V 6000 3528 50  0000 L CNN
F 2 "" H 6000 3400 50  0001 C CNN
F 3 "~" H 6000 3400 50  0001 C CNN
	1    6000 3400
	0    1    1    0   
$EndComp
Connection ~ 6000 3400
Wire Wire Line
	6000 3400 6000 3300
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 61DE2549
P 1800 4750
F 0 "#FLG0104" H 1800 4825 50  0001 C CNN
F 1 "PWR_FLAG" H 1800 4923 50  0000 C CNN
F 2 "" H 1800 4750 50  0001 C CNN
F 3 "~" H 1800 4750 50  0001 C CNN
	1    1800 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J3
U 1 1 61DEAF09
P 2850 3000
F 0 "J3" H 2900 3217 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 2900 3126 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x02_P1.27mm_Vertical_SMD" H 2850 3000 50  0001 C CNN
F 3 "~" H 2850 3000 50  0001 C CNN
	1    2850 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 3450 3050 3450
Wire Wire Line
	2650 3000 2450 3000
Wire Wire Line
	2650 3100 2450 3100
Wire Wire Line
	3150 3100 3400 3100
Wire Wire Line
	3150 3000 3400 3000
Text Label 2500 3000 0    50   ~ 0
TX1
Text Label 3200 3000 0    50   ~ 0
RX1
$Comp
L power:GND #PWR0117
U 1 1 61E0A65D
P 2450 3100
F 0 "#PWR0117" H 2450 2850 50  0001 C CNN
F 1 "GND" V 2455 2972 50  0000 R CNN
F 2 "" H 2450 3100 50  0001 C CNN
F 3 "" H 2450 3100 50  0001 C CNN
	1    2450 3100
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0118
U 1 1 61E0AB9F
P 3400 3100
F 0 "#PWR0118" H 3400 2950 50  0001 C CNN
F 1 "+3V3" V 3415 3228 50  0000 L CNN
F 2 "" H 3400 3100 50  0001 C CNN
F 3 "" H 3400 3100 50  0001 C CNN
	1    3400 3100
	0    1    1    0   
$EndComp
$EndSCHEMATC

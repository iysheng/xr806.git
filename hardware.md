#### 爲了驅動大連佳顯的一款墨水屏幕，開發了一個擴展板

整個硬件設計使用 Kicad 進行開發，邊框使用 Librecad 繪製。

具有的功能
- [x] 紅外遙控
- [x] UART1 串口調試
- [x] 預留一組 SPI 接口

#### 3D 視圖

頂視圖

---

![top](assets/xr806_extend_top.png)


底視圖

---

![bottom](assets/xr806_extend_bottom.png)


#### 实际视图

![r0](assets/r0.jpg)

![r1](assets/r1.jpg)

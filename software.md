#### 墨水显示屏软件开发流程

这次使用的是大连佳显公司的一款墨水屏 [GDEH042Z96](https://www.e-paper-display.cn/products_detail/productId%3d532.html), 特此强调一点，这款墨水屏的刷新周期大概在 **16s** 附近。

这次算是第一次近距离接触鸿蒙，拿到板子，刚开始的时候在学习系统的整个构建过程，大概有一周左右的时间，输出了一点内容：[請教現在的工程是如何生成 xr_system.img 的？](https://bbs.aw-ol.com/topic/842/%E8%AB%8B%E6%95%99%E7%8F%BE%E5%9C%A8%E7%9A%84%E5%B7%A5%E7%A8%8B%E6%98%AF%E5%A6%82%E4%BD%95%E7%94%9F%E6%88%90-xr_system-img-%E7%9A%84?_=1641376956006&lang=en-GB)

大概了解了整个系统的构建过程后，开始着手设计一个底板用用来驱动墨水显示屏。现在显示屏幕的驱动已经可以正常工作了，展示一个 demo 效果图。

---
![flag](assets/flag.jpg)

首先要考虑的一个问题是在哪里启动这个程序，比较合适呢？我选择了在 ``device/xradio/xr806/ohosdemo/`` 目录创建一个新的目录 ``screen_demo``， 通过修改 ``device/xradio/xr806/ohosdemo/`` 的 ``BUILD.gn`` 来编译构建这个线程。具体如下：
```
├── screen_demo
│   ├── BUILD.gn
│   ├── include
│   │   ├── flag.h
│   │   ├── flag_r.h
│   │   ├── GDEH042Z96.h
│   │   ├── jx_demo.h
│   │   └── screen_demo.h
│   └── src
│       ├── GDEH042Z96.c
│       └── main.c
```
其中，相关的代码我都托管在 gitee 上了。[iysheng / devboard_device_allwinner_XR806](https://gitee.com/iysheng/devboard_device_allwinner_xr806.git)， 记得是 **notes** 分支哦。
接下来就到具体的驱动开发了，涉及到的模块主要由：SPI、GPIO。在没有完整数据手册的前提下，怎么着手呢，只能去代码中查找了。具体外设的使用方法都以代码的形式展示了出来，具体的代码在路径``device/xradio/xr806/xr_skylark/project/example``：
```
├── adc
├── airkiss
├── audio_play
├── audio_play_and_record
├── audio_record
├── audio_reverb
├── ble
├── blink
├── button
├── ce
├── cplusplus
├── dhcpd
├── dma
├── efpg
├── fdcm
├── flash
├── gpio
├── httpc
├── i2c
├── i2s
├── ir
├── keyscan
├── lpuart
├── matrix_button
├── mqtt
├── nopoll
├── pm
├── pwm
├── rtc
├── secure_boot
├── smartcard
├── smartconfig
├── sntp
├── soft_ap_config
├── spi
├── timer
├── uart
├── voice_print
├── wdg
├── wlan
├── wlan_csfc
└── wlan_low_power
```
这里包含了涉及到很多外设。

**备注**：
1. 在此说明一下，使用这个驱动，在使用 Image2Lcd 软件取模的时候，为了适配比较好的显示效果，我是这样设置的：

---
![image2lcd](assets/image2lcd.png)

2. 具体的实际效果，在 [硬件設計](https://gitee.com/iysheng/xr806/blob/master/hardware.md#实际视图) 章节

---
**screen_app** 设计。

解决的问题：为了改善在家做月子时候，家人希望时不时地进屋里看小孩，但是如果这时候小孩子在睡觉，或者在吃奶，如果贸然地进来是不是很不礼貌，纵使是敲门，这时候会不会吵到小孩子呢。如果有一个小看板可以放在客厅或者门口位置，孩子妈妈可以通过手机遥控修改看板上的内容，及时更新小孩子的状态，是不是可以减少贸然的尴尬呢？

实现的方法：针对上述问题，我基于 XR806 设计了一款比较小巧的**母婴看板**设备。这个设备支持手机通过微信连连遥控看板的内容，及时更新小孩子的状态。

整个项目的结构图示如下：
<img src="assets/xr806-Elink.png" alt="elink" style="zoom:80%;" />

其中 app 部分的开发参考了如下几个文件：
* device/xradio/xr806/xr_skylark/project/example/mqtt/main.c
* device/xradio/xr806/xr_skylark/project/example/wlan/main.c
* device/xradio/xr806/xr_skylark/src/cjson/json_test.c
* device/xradio/xr806/xr_skylark/project/common/cmd/cmd_json.c

分别实现了 MQTT 的移植以及 cJSON 格式数据解析

特别地，通过参考 `wlan/main.c` 文件，实现了上电自动连接热点的功能。相关的代码都在仓库[https://gitee.com/iysheng/devboard_device_allwinner_xr806.git](https://gitee.com/iysheng/devboard_device_allwinner_xr806.git)

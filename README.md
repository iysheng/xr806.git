![ho](./assets/HO.png)


#### 該倉庫記錄學習 XR806 的筆記

---

* [構建過程](https://gitee.com/iysheng/xr806/blob/master/build.md)
* [硬件設計](https://gitee.com/iysheng/xr806/blob/master/hardware.md)
* [软件設計](https://gitee.com/iysheng/xr806/blob/master/software.md)


#### 官方資料

---

* [XR806 Datasheet](https://gitee.com/iysheng/xr806/blob/master/docs/XR806_Datasheet.pdf)
* [XR806_PIN_MUX](https://gitee.com/iysheng/xr806/blob/master/docs/XR806_PIN_MUX.pdf)
* [XR806开发板原理图](https://gitee.com/iysheng/xr806/blob/master/docs/XR806开发板原理图.pdf)
* [XR806开发板引脚功能表](https://gitee.com/iysheng/xr806/blob/master/docs/开发板引脚功能表.xls)
* [XR806鸿蒙无线模组原理图](https://gitee.com/iysheng/xr806/blob/master/docs/鸿蒙无线模组原理图.pdf)
* [GDEH042Z96](https://gitee.com/iysheng/xr806/blob/master/docs/GDEH042Z96.pdf)

